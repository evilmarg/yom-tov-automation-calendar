# Yom Tov Automation Calendar

This project is devoted to making Yom Tov home automation as easy as shabbat home automation by building custom automation calendars that are Yom Tov-aware, and documenting how to use those to drive your home automation projects.

You can find complete instructions on the project [wiki](https://gitlab.com/evilmarg/yom-tov-automation-calendar/-/wikis/Instructions).