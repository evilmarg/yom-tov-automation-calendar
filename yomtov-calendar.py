###########################################
# Copyright (C) 2019 Jonathan Margulies 
###########################################

# Tested on Python 2.7 and 3.8

import csv
from datetime import datetime, timedelta, tzinfo
import requests
from lxml import html

start_year = 2020
end_year = 2023
current_year = start_year
zip = '60076'

while current_year < end_year:
  csvfile = []
  url = 'https://www.hebcal.com/hebcal/?v=1&maj=on&i=off&year=' + str(current_year) + '&month=x&yt=G&lg=s&c=on&geo=zip&zip=' + zip + '&city=&geonameid=&b=18&m=50&.s=Create+Calendar#ol-csv-body'
  response = requests.get(url)

  #parse response for CSV download URL
  tree = html.fromstring(response.content)
  csv_link = tree.get_element_by_id('dl-ol-csv-usa')

 for element, attribute, link, pos in html.iterlinks(csv_link):
    csv_response = requests.get(link)

    content = csv_response.content.decode('utf-8')

    reader = csv.DictReader(content.splitlines())
    for row in reader:
      csvfile.append(row)

  ########################## modify CSV to control lights
  holidays = []

  with open('light_automation_' + zip + '_' + str(current_year) + '_' + str(current_year + 3) + '.csv', 'w') as output:
    writer = None

    for row in csvfile:
      if not writer:
        writer = csv.DictWriter(output, fieldnames=row.keys())
        writer.writeheader()

      if row['Subject'] == "Candle lighting":
        row['Subject'] = "Erev Shabbat/YT"
        start_date_tuple = row['Start Date'].split("/")
        start_date = datetime(int(start_date_tuple[2]), int(start_date_tuple[0]), int(start_date_tuple[1]))
        end_date = start_date + timedelta(days=1)

        night_end = "01:00 AM"
        morning_start = "06:45 AM"
        afternoon_start = "11:45 AM"

        row['End Date'] = str(end_date.month) + "/" + str(end_date.day) + "/" + str(end_date.year)
        row['End Time'] = night_end

        writer.writerow(row)

        row['Subject'] = "Shabbat/YT Morning"
        row['Start Date'] = str(end_date.month) + "/" + str(end_date.day) + "/" + str(end_date.year)
        row['Start Time'] = morning_start
        row['End Time'] = afternoon_start

        writer.writerow(row)

        row['Subject'] = "Shabbat/YT Afternoon"
        row['Start Date'] = str(end_date.month) + "/" + str(end_date.day) + "/" + str(end_date.year)
        row['Start Time'] = afternoon_start
        row['End Time'] = "10:00 PM"

        writer.writerow(row)

  current_year += 4
